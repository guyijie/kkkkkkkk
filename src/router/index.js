import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/views/home/index'
import LoginView from  '@/views/login/index'
import WelcomeView from '@/views/welcome/index'
import ChatPage from "@/components/ChatPage/ChatPage";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: '/chat/',
    routes:[
        {
            path:'/',
            redirect:'/login'
        },
        {
            path: '/login',
            name: '登录页面',
            component: LoginView,
        },
        {
            path: '/home',
            name: '用户页面',
            component: HomeView,
            children: [
                {
                    path: 'messages/messageChat/:chatId',
                    name: 'messageDialog',
                    component: ChatPage
                },
                {
                    path: '/welcome',
                    name: 'welcome',
                    component: WelcomeView
                }
            ]
        }
    ]
})

export default router