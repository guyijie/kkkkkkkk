import request from '@/utils/request'
import Qs from 'qs'

export const listUserChannels = (userName) =>
    request({
        url: '/api/userChannels',
        method: 'GET',
        params: {
            userName: userName,
        }
    })

export const getChatById = (chatId) =>
    request({
        url: '/api/getChatById',
        method: 'GET',
        params: {
            chatId: chatId,
        }
    })

export const getChatMemberById = (chatId) =>
    request({
        url: '/api/getChatMemberById',
        method: 'GET',
        params: {
            chatId: chatId,
        }
    })

export const getMsgRecorder = (chatId, times) =>
    request({
        url: '/api/getMsgRecorder',
        method: 'GET',
        params: {
            chatId: chatId,
            times: times,
        }
    })

export const deleteChat = (chatId, userName) =>
    request({
        url: '/api/chat/deleteChat',
        method: 'GET',
        params: {
            chatId: chatId,
            userName: userName,
        }
    })

export const quitChat = (chatId, userName) =>
    request({
        url: '/api/chat/quitChat',
        method: 'GET',
        params: {
            chatId: chatId,
            userName: userName,
        }
    })

export const changeChatName = (chatId, userName, chatName) =>
    request({
        url: '/api/chat/changeChatName',
        method: 'GET',
        params: {
            chatId: chatId,
            userName: userName,
            chatName: chatName
        }
    })

export const createNewChat = (memberList, chatName) =>
    request({
        url: '/api/chat/createNewChat',
        method: 'POST',
        params: {
            memberList: Qs.stringify({memberList: memberList}, {indices: false}),
            chatName: chatName
        }
    })

