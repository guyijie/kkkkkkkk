import request from '@/utils/request'
import qs from 'qs'

export const login = (username, password) =>
    request({
        url: '/api/login',
        method: 'post',
        data: qs.stringify({
            username,
            password
        })
    })
export const registerUser = (username, nickname, password) =>
    request({
        url: '/api/register',
        method: 'POST',
        data: qs.stringify({
            username,
            nickname,
            password
        })
    })