import request from '@/utils/request'

export const getUserById = (userId) =>
    request({
        url: '/api/getUserById',
        method: 'GET',
        params: {
            userName: userId,
        }
    })

export const getAllUser = (userName) =>
    request({
        url: '/api/getAllUser',
        method: 'GET',
        params: {
            userName: userName,
        }
    })

export const changeUserInfo = (userInfo) =>
    request({
        url: '/api/changeUserInfo',
        method: 'post',
        data: userInfo
    })

// export const changeUserInfoTest = (userInfo) =>
//     request({
//         url: '/api/changeUserInfoTest',
//         method: 'post',
//         headers: { 'Content-Type': 'multipart/form-data' },
//         data: userInfo
//     })




