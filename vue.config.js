// const webpack = require("webpack")
// const CompressionWebpackPlugin = require('compression-webpack-plugin')
// const productionGzipExtensions = ['js', 'css']

module.exports = {
    // configureWebpack: {
    //     plugins: [
    //         new webpack.ProvidePlugin({
    //             $:"jquery",
    //             jQuery:"jquery",
    //             "windows.jQuery":"jquery"
    //         }),
    //         // 下面是下载的插件的配置
    //         new CompressionWebpackPlugin({
    //             algorithm: 'gzip',
    //             test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
    //             threshold: 10240,
    //             minRatio: 0.8
    //         }),
    //         new webpack.optimize.LimitChunkCountPlugin({
    //             maxChunks: 5,
    //             minChunkSize: 100
    //         })
    //     ]
    // },
    // publicPath: './',
    devServer: {
        open: true, //是否自动弹出浏览器页面
        host: "localhost",
        port: '8080',
        https: true,
        hotOnly: false  ,
        proxy: {
            '/api': {
                target: 'https://121.199.4.228:8081/', //API服务器的地址(由于此处我nodejs后台用了路由，所以+了api)，正常不加
                // target: 'https://localhost:8081/',
                ws: true,  //代理websockets
                changeOrigin: true, // 虚拟的站点需要更管origin
                pathRewrite: {   //重写路径 比如'/api/aaa/ccc'重写为'/aaa/ccc'
                    '^/api': '/api'
                }
            }
        },
    }
}